<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 02-05-2021
  Time: 10:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
     minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi">



    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
            crossorigin="anonymous"
    />

    <link
            rel="stylesheezt"
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
            crossorigin="anonymous"
    />
    <style><%@include file="../stylesheet/style.css"%></style>
    <title>FORM</title>
</head>
<body>




<div class="header sticky-top bg-white">
    <nav class="navbar navbar-expand-lg ">
        <a class="navbar-brand h2" href="/user/home">USERINFO!</a>
        <button class="navbar-toggler navbar-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav ml-lg-4">
                <li class="nav-item active">
                    <a class="nav-link h4" href="/user/home">HOME <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link h4" href="/user/addForm">AddUser <span class="sr-only">(current)</span></a>
                </li>

            </ul>
        </div>

    </nav>
    <hr>
</div>
<div class="container">
    <h1 class="header-title" style="text-align: center;">ADD DETAILS</h1>
    <form   action="addData" method="POST">

<%--        <label class="form-label" for="customFile">IMAGE:</label><br>--%>
<%--        <input type="file"  id="customFile" />--%>
<%--        <br/>--%>
    <div class="form-group">
        <label>URL:</label>
        <input type="text" class="form-control" name="url" placeholder="Enter url"  required>
    </div>
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control" name="name" placeholder="Enter name"  required>
        </div>
        <div class="form-group">
            <label>AGE:</label>
            <input type="number" class="form-control" name="age" placeholder="Enter age"  required>
        </div>
        <div class="form-group">
            <label>PHONE NUMBER:</label>
            <input type="number" class="form-control" name="pnumber" placeholder="Enter phone number"  required>
        </div>
        <div class="form-group">
            <label>STATE:</label>
            <input type="text" class="form-control" name="state" placeholder="Enter state"  required>
        </div>


        <input type="submit" class="btn btn-primary b2" />
    </form>

</div>
<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
        integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
        crossorigin="anonymous"
></script>
</body>
</html>

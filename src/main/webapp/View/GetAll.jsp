<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 04-05-2021
  Time: 11:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<link>

<meta charset="utf-8">
<meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
     minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi">

<link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
        crossorigin="anonymous"
/>

<link
        rel="stylesheezt"
        href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
        crossorigin="anonymous"
/>
    <link src="../stylesheet/style.css"/>
<title>TABLE</title>
</head>
<body>
<div  class="container">
    <table class="table table-striped mt-5" >
        <tr>

            <th>NAME</th>
            <th>ROLLNO</th>
            <th>PHNNO</th>
            <th>STATE</th>
        </tr>
        <c:forEach var="task" items="${tasks}">

            <tr>

                <td>${task.name}</td>
                <td>${task.age}</td>
                <td>${task.pnumber}</td>
                <td>${task.state}</td>
<%--                <td><a href="/View/EditForm.jsp?u=${task.id}">EDIT</a></td>--%>
                <td><a href="/form/formredirect/${task.id}">EDIT</a></td>
                <td><a href="/form/delete/${task.id}">DELETE</a></td>


            </tr>
        </c:forEach>

    </table></div>
<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
        integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
        crossorigin="anonymous"
></script>
</body>
</html>

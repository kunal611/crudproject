<%--&lt;%&ndash;--%>
<%--  Created by IntelliJ IDEA.--%>
<%--  User: DELL--%>
<%--  Date: 07-05-2021--%>
<%--  Time: 11:38 AM--%>
<%--  To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
<%--    <meta charset="utf-8">--%>
<%--    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,--%>
<%--     minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-density ,dpi=device-dpi">--%>


<%--    <link--%>
<%--            rel="stylesheet"--%>
<%--            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"--%>
<%--            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"--%>
<%--            crossorigin="anonymous"--%>
<%--    />--%>
<%--    <title>HOME PAGE</title>--%>
<%--</head>--%>
<%--<body style="background-color: #F1EFF5;">--%>

<%--<div>--%>

<%--    <nav class="navbar navbar-expand-lg " >--%>
<%--        <a class="navbar-brand h2" href="/user/home">USERINFO!</a>--%>
<%--        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">--%>
<%--            <span class="navbar-toggler-icon"></span>--%>

<%--        </button>--%>

<%--        <div class="collapse navbar-collapse" id="navbarSupportedContent">--%>
<%--            <ul class="navbar-nav ml-lg-4">--%>
<%--                <li class="nav-item active">--%>
<%--                    <a class="nav-link h4" href="/user/home">HOME <span class="sr-only">(current)</span></a>--%>
<%--                </li>--%>
<%--                <li class="nav-item active">--%>
<%--                    <a class="nav-link h4" href="/user/addForm">AddUser <span class="sr-only">(current)</span></a>--%>
<%--                </li>--%>

<%--            </ul>--%>

<%--        </div>--%>
<%--    </nav>--%>

<%--    <div  class="container mt-5">--%>
<%--        <div class="row">--%>

<%--            <c:forEach var="task" items="${tasks}">--%>
<%--                <div class="col-3">--%>


<%--                <div class="card container" style="width: 18rem;">--%>
<%--                    <img class="card-img-top" src="${task.url}" alt="Card image cap" height="200" width="200">--%>
<%--                    <div class="card-body">--%>
<%--                        <h5 class="card-title">${task.name}</h5>--%>
<%--                        <h6 class="card-subtitle mb-2 ">AGE:${task.age}</h6>--%>
<%--                        <h6 class="card-text">--%>
<%--                            PHONE NUMBER:${task.pnumber}--%>

<%--                        </h6>--%>
<%--                        <h6 class="card-text">--%>
<%--                            STATE:${task.state}--%>
<%--                        </h6>--%>
<%--                        <a href="/user/formredirect/${task.id}" class="card-link">EDIT</a>--%>
<%--                        <a href="/user/delete/${task.id}" class="card-link">DELETE</a>--%>
<%--                    </div>--%>
<%--                </div>--%>

<%--                </div>--%>
<%--            </c:forEach>--%>
<%--        </div>--%>
<%--        </div>--%>

<%--</div>--%>


<%--<script--%>
<%--        src="https://code.jquery.com/jquery-3.6.0.min.js"--%>
<%--        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="--%>
<%--        crossorigin="anonymous"--%>
<%--></script>--%>
<%--<script--%>
<%--        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"--%>
<%--        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"--%>
<%--        crossorigin="anonymous"--%>
<%--></script>--%>
<%--<script--%>
<%--        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"--%>
<%--        integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"--%>
<%--        crossorigin="anonymous"--%>
<%--></script>--%>
<%--</body>--%>
<%--</html>--%>
<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 07-05-2021
  Time: 11:38 AM
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
     minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-density ,dpi=device-dpi">

    <script src="https://use.fontawesome.com/c970c9d5cb.js"></script>

    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
            crossorigin="anonymous"
    />
    <title>HOME PAGE</title>
</head>
<body style="background-color: #F1EFF5;">

<div>
    <div class="header sticky-top bg-white align-content-center">
        <nav class="navbar navbar-expand-lg ">
            <a class="navbar-brand h2" href="/user/home">USERINFO!</a>
            <button class="navbar-toggler navbar-light" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">

                <ul class="navbar-nav ml-lg-4 ">


                    <li class="nav-item active ">
                        <a class="nav-link h4" href="/user/home">HOME <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active ">
                        <a class="nav-link h4 " href="/user/addForm">AddUser <span class="sr-only">(current)</span></a>
                    </li>

                </ul>
            </div>

        </nav>
        <hr>
    </div>
    <div class="container mt-5">
        <div class="row">

            <c:forEach var="task" items="${tasks}">
                <div class="col-3 mb-3">

                    <div class="card" style=" box-shadow: 0 2px 6px 0 #1a181e66">
                        <img class="card-img-top" src="${task.url}" alt="Card image cap" height="180" width="286">
                        <div class="card-body">
                            <div class="d-flex justify-content-end">
                                <a href="/user/formredirect/${task.id}" class="card-link"><i
                                        class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a>
                                <a href="/user/delete/${task.id}" class="card-link"><i class="fa fa-trash-o fa-lg"
                                                                                       aria-hidden="true"></i></a>
                            </div>
                            <h5 class="card-title">${task.name}</h5>
                            <h6 class="card-subtitle mb-2 ">AGE:${task.age}</h6>
                            <h6 class="card-text">
                                STATE:${task.state}
                            </h6>
                            <h6 class="card-text">
                                <i class="fa fa-mobile fa-lg" aria-hidden="true"></i> ${task.pnumber}

                            </h6>


                        </div>
                    </div>

                </div>
            </c:forEach>
        </div>
    </div>

</div>


<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
        integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
        crossorigin="anonymous"
></script>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 06-05-2021
  Time: 04:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
     minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi">


    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
            crossorigin="anonymous"
    />

    <link
            rel="stylesheezt"
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
            crossorigin="anonymous"
    />

</head>
<body>

<div class="header">
    <h1 class="header-title" style="text-align: center;">DETAILS</h1>
</div>
<div class="container">
    <form >
        <%--        <div class="form-group">--%>
        <%--            <label>Id:</label>--%>
        <%--            <input type="number" class="form-control" value="${tasks.id}" name="id" >--%>
        <%--        </div>--%>
        <%--        value=<%=request.getParameter("u") %--%>
            <div class="form-group" >
                <label>ID:</label>
                <input type="text" class="form-control" name="id"  id="id" value="${tasks.id}" required>
            </div>
        <div class="form-group" >
            <label>Name:</label>
            <input type="text" class="form-control" name="name"  id="name" value="${tasks.name}" required>
        </div>
        <div class="form-group">
            <label>AGE:</label>
            <input type="number" class="form-control" name="age" id="age" value="${tasks.age}"  required>
        </div>
        <div class="form-group">
            <label>PHONE NUMBER:</label>
            <input type="number" class="form-control" name="pnumber" id="pnumber" value="${tasks.pnumber}"  required>
        </div>
        <div class="form-group">
            <label>STATE:</label>
            <input type="text" class="form-control" name="state" id="state" value="${tasks.state}"  required>
        </div>
<%--        <button type="submit" class="btn btn-primary b2">UPDATE</button>--%>
            <button type="button" class="btn btn-primary b2" onclick="editUser()">UPDATE</button>
    </form>


</div>

<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
        integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
        crossorigin="anonymous"
></script>
<script src="/js/User.js"></script>
</body>
</html>
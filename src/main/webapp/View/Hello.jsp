<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
     minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi">

    <style><%@include file="/stylesheet/style.css"%></style>

</head>
<body>

<table  class="table table-bordered table-striped"  align="center" border="1">
<tr>
            <th>ID</th>
            <th>ZIP</th>
            <th>CITY</th>
            <th>STATE</th>
            <th>LATITUDE</th>
            <th>LONGITTUDE</th>
            <th>TIMEZONE</th>
            <th>DST</th>
        </tr>
    <c:forEach var="j" begin="0" end="43204">

        <tr>
            <td>${task[j].id}</td>
             <td>${task[j].zip}</td>
              <td>${task[j].city}</td>
               <td>${task[j].state}</td>
               <td>${task[j].latitude}</td>
               <td>${task[j].longitude}</td>
               <td>${task[j].timezone}</td>
               <td>${task[j].dst}</td>


        </tr>
    </c:forEach>
</table>
<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"
></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
        integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
        crossorigin="anonymous"
></script>
</body>

</html>
package com.java.crudProject.repository;

import com.java.crudProject.model.FormInfo;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormDetailsRepository extends JpaRepository<FormInfo, Long> {


}
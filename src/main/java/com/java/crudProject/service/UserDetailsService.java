package com.java.crudProject.service;



import com.java.crudProject.model.UserDetails;
import com.java.crudProject.repository.UserDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserDetailsService {

    @Autowired
    UserDetailsRepository userDetailsRepository;

    public UserDetails adddata(UserDetails info)  {

        UserDetails result= this.userDetailsRepository.save(info);
        return result;

    }


    public List<UserDetails> findAll() {
        List<UserDetails> bes = (List<UserDetails>) userDetailsRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        return bes;
    }

    public void updateRecord(UserDetails info) {


        userDetailsRepository.save(info);
    }


    public void deleteById(long id) {
        userDetailsRepository.deleteById(id);
    }


    public Optional<UserDetails> findById(long id) {
       Optional<UserDetails> res= userDetailsRepository.findById(id);
       return res;

    }
}

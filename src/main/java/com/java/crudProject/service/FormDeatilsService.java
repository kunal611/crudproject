package com.java.crudProject.service;

import com.java.crudProject.model.FormInfo;
import com.java.crudProject.repository.FormDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class FormDeatilsService {

    @Autowired
    FormDetailsRepository formDetailsRepository;

    public FormInfo adddata(FormInfo info)  {

        FormInfo result= this.formDetailsRepository.save(info);
        return result;

    }


    public List<FormInfo> findAll() {
        List<FormInfo> bes = (List<FormInfo>) formDetailsRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        return bes;
    }

    public void updateRecord(FormInfo info) {


        formDetailsRepository.save(info);
    }


    public void deleteById(long id) {
        formDetailsRepository.deleteById(id);
    }


    public Optional<FormInfo> findById(long id) {
        Optional<FormInfo> res= formDetailsRepository.findById(id);
        return res;

    }
}

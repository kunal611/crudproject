package com.java.crudProject.service;

import com.java.crudProject.model.UserDetails;
import com.java.crudProject.repository.UserDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import java.util.Optional;

@Service
public class AjaxService {
   @Autowired
    UserDetailsRepository userDetailsRepository;
    public UserDetails saveUser( String name, int age, String pnumber, String state) {
        UserDetails inf=new UserDetails();


        inf.setName(name);
        inf.setAge(age);
        inf.setPnumber(pnumber);
        inf.setState(state);
        userDetailsRepository.save(inf);
        return inf;
    }
    public List<UserDetails> findAll() {
        List<UserDetails> bes = (List<UserDetails>) userDetailsRepository.findAll();
        return bes;
    }
    public Optional<UserDetails> findById(long id) {
        Optional<UserDetails> inf=userDetailsRepository.findById(id);
        return inf;
    }
    public UserDetails updateRecord(UserDetails info) {


      return  userDetailsRepository.save(info);
    }
    public void deleteById(long id) {
        userDetailsRepository.deleteById(id);
    }
}

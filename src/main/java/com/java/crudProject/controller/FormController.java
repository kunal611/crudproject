package com.java.crudProject.controller;



import com.java.crudProject.model.UserDetails;
import com.java.crudProject.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@RequestMapping("/form")
public class FormController {


    @Autowired
    UserDetailsService userDetailsService;


    @RequestMapping("/user")
    public String Form() {

        return "Form";
    }

    @RequestMapping(value = "/showData", method = RequestMethod.GET)
    public ModelAndView taskList() {

        ModelAndView modelAndView=new ModelAndView();

        modelAndView.addObject("tasks",userDetailsService.findAll());
        modelAndView.setViewName("GetAll");
        return modelAndView;
    }

@RequestMapping("/addData")
public String addData(@RequestParam("name") String name,@RequestParam("age") int age,@RequestParam("pnumber") String pnumber,@RequestParam("state") String state) {
        UserDetails details=new UserDetails();


        details.setName(name);
        details.setAge(age);
        details.setPnumber(pnumber);
        details.setState(state);

UserDetails b=this.userDetailsService.adddata(details);
    System.out.print(b);
    return "Form";
}

    @RequestMapping(value="/formredirect/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();



       Optional<UserDetails> info= userDetailsService.findById(id);

//        modelAndView.addObject("val",id);
        modelAndView.addObject("tasks",info.get());
        modelAndView.setViewName("EditForm");
        return modelAndView;
    }
    @PostMapping(value="/updateData/{id}")
    public String updateIt(@PathVariable long id,@RequestParam(required = false) String name,@RequestParam(required = false) int age,@RequestParam(required = false) String pnumber,@RequestParam(required = false) String state) {
        UserDetails details=new UserDetails();
        if(id!=0)
            details.setId(id);
        if(name!=null)
            details.setName(name);
        if(age!=0)
            details.setAge(age);
        if(pnumber!=null)
            details.setPnumber(pnumber);
        if(state!=null)
            details.setState(state);

        userDetailsService.updateRecord(details);

        return "redirect:/form/showData";
    }
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable long id) {

        userDetailsService.deleteById(id);
        return "redirect:/form/showData";

    }

}












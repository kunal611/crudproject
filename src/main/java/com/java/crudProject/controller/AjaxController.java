package com.java.crudProject.controller;


import com.java.crudProject.model.AreaInfo;
import com.java.crudProject.model.UserDetails;
import com.java.crudProject.service.AjaxService;
import com.java.crudProject.service.UserDetailsService;
        import org.json.JSONException;
        import org.json.JSONObject;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/ajax")
public class AjaxController {
    @Autowired
    AjaxService dataService;

    @GetMapping("/get")
    public String indexform()
    {

        return "index";
    }



    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public @ResponseBody List<UserDetails> taskList() {
System.out.print("hhh");

        List<UserDetails> res=dataService.findAll();
        return res;

    }

    @GetMapping("/getAll")
    public String index()
    {

        return "GetAllAjax";
    }

    @PostMapping("/saveUser")
    public @ResponseBody String saveUser(@RequestParam(required = false) String name,@RequestParam(required = false) int age,@RequestParam(required = false)
            String pnumber,@RequestParam(required = false) String state) throws JSONException
    {
        UserDetails inf=dataService.saveUser(name,age,pnumber,state);
        JSONObject obj=new JSONObject();


        obj.put("name",inf.getName());
        obj.put("age",inf.getAge());
        obj.put("pnumber",inf.getPnumber());
        obj.put("state",inf.getState());
System.out.print(inf.getName()+inf.getAge()+inf.getPnumber()+inf.getState());
        return  obj.toString();

    }
    @RequestMapping(value="/redirectajax/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();
        Optional<UserDetails> inf=dataService.findById(id);
        modelAndView.addObject("tasks",inf.get());
        modelAndView.setViewName("EditFormAjax");
        return modelAndView;
    }

    @RequestMapping(value="/editUser",method=RequestMethod.POST)
    public @ResponseBody UserDetails updateIt(@RequestParam long id,
                           @RequestParam(required=false) String name,@RequestParam(required=false) int age,@RequestParam(required=false) String pnumber,@RequestParam(required=false) String state) {
        UserDetails details=new UserDetails();
        if(id!=0)
            details.setId(id);
        if(name!=null)
            details.setName(name);
        if(age!=0)
            details.setAge(age);
        if(pnumber!=null)
            details.setPnumber(pnumber);
        if(state!=null)
            details.setState(state);

      UserDetails res=  dataService.updateRecord(details);

        return res;
    }
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable Long id) {
        System.out.println(id);

        dataService.deleteById(id);
        return "redirect:/ajax/get";
    }

}
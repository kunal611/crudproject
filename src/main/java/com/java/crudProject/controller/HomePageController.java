package com.java.crudProject.controller;


import com.java.crudProject.model.FormInfo;
import com.java.crudProject.model.UserDetails;
import com.java.crudProject.service.FormDeatilsService;
import com.java.crudProject.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@RequestMapping("/user")
public class HomePageController {

    @Autowired
    FormDeatilsService formDeatilsService;






    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView taskList() {

        ModelAndView modelAndView=new ModelAndView();

        modelAndView.addObject("tasks",formDeatilsService.findAll());
        modelAndView.setViewName("HomePage");
        return modelAndView;
    }

    @RequestMapping("/addForm")
    public String Form() {

        return "AddForm";
    }
    @RequestMapping("/addData")
    public String addData(@RequestParam("url") String url,@RequestParam("name") String name,@RequestParam("age") int age,@RequestParam("pnumber") String pnumber,@RequestParam("state") String state) {
        FormInfo details=new FormInfo();

        details.setUrl(url);
        details.setName(name);
        details.setAge(age);
        details.setPnumber(pnumber);
        details.setState(state);

        FormInfo b=this.formDeatilsService.adddata(details);
        System.out.print(b);
        return "AddForm";
    }

    @RequestMapping(value="/formredirect/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();



        Optional<FormInfo> info= formDeatilsService.findById(id);

//        modelAndView.addObject("val",id);
        modelAndView.addObject("tasks",info.get());
        modelAndView.setViewName("EditHome");
        return modelAndView;
    }
    @PostMapping(value="/updateData/{id}")
    public String updateIt(@PathVariable long id,@RequestParam(required = false) String url,@RequestParam(required = false) String name,@RequestParam(required = false) int age,@RequestParam(required = false) String pnumber,@RequestParam(required = false) String state) {
        FormInfo details=new FormInfo();
        if(id!=0)
            details.setId(id);
        if(url!=null)
        details.setUrl(url);
        if(name!=null)
            details.setName(name);
        if(age!=0)
            details.setAge(age);
        if(pnumber!=null)
            details.setPnumber(pnumber);
        if(state!=null)
            details.setState(state);

        formDeatilsService.updateRecord(details);

        return "redirect:/user/home";
    }
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable long id) {

        formDeatilsService.deleteById(id);
        return "redirect:/user/home";

    }

}

